import { test } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts index');

test('visiting user contacts route displays all contacts', function(assert) {
  server.createList('contact', 5);

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact').length, 5, 'All contacts display');
  });
});

test('user contacts list is sorted by name', function(assert) {
  server.create('contact', { name: 'John Armstrong' });
  server.create('contact', { name: 'John Doe' });
  server.create('contact', { name: 'Jane Doe' });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Jane Doe', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(1) .test-contact-name', 'John Armstrong', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(2) .test-contact-name', 'John Doe', 'Contact list is sorted alphabetically');
  });
});

test('displays info for a contact in each row', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryEmailAddress: 'smith@example.com',
    primaryPhoneNumber: '+1 555-555-5252'
  });
  server.createList('email-address', 2, { contactId: contact.id });
  server.create('phone-number', { contactId: contact.id });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Ann Smith', 'Contact row displays contact name');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', 'smith@example.com', 'Contact row displays contact primary email');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', '(+2)', 'Displays additional email count');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '+1 555-555-5252', 'Contact row displays primary phone');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '(+1)', 'Displays additional phone count');
  });
});

test('name links to edit for a contact', function(assert) {
  let contact = server.create('contact', { id: 1, name: 'John Doe' });

  visit('/contacts');
  click('.test-contact:eq(0)');

  andThen(function() {
    assert.equal(currentURL(), `/contacts/${contact.id}`);
  });
});

test('primary email is a mailto: link', function(assert) {
  server.create('contact', { primaryEmailAddress: 'smith@example.com' });

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact-primary-email a').attr('href'), 'mailto:smith@example.com');
  });
});

test('primary phone is a tel: link', function(assert) {
  server.create('contact', { primaryPhoneNumber: '(818) 123-4567' });

  visit('/contacts');

  andThen(function() {
    console.log('TEST', find('.test-contact-primary-phone a').attr('href') === 'tel:(818) 123-4567');
    assert.equal(find('.test-contact-primary-phone a').attr('href'), 'tel:(818) 123-4567');
  });
});
