import { test, skip } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts edit');

test('Edit contact displays contact current info', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });
  server.create('phone-number', { value: '+15555550002', contactId: contact.id });
  server.create('email-address', { value: 'smith@example.org', contactId: contact.id });
  server.create('email-address', { value: 'asmith@example.edu', contactId: contact.id });

  visitEditContact(contact);

  andThen(function() {
    assert.contains('.test-contact-name', 'Ann Smith', 'Contact name is displayed');
    assert.contains('.test-contact-phone', '+15555555252', 'Contact primary phone is displayed');
    assert.contains('.test-contact-phone', '+15555550002', 'Contact alternate phone is displayed');
    assert.contains('.test-contact-email', 'smith@example.com', 'Contact primary email is displayed');
    assert.contains('.test-contact-email', 'smith@example.org', 'Contact alternate email is displayed');
    assert.contains('.test-contact-email', 'asmith@example.edu', 'Contact work email is displayed');
  });
});

test('Edit contact allows editing contact info', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });

  visitEditContact(contact);

  click('.test-edit-button');
  fillIn('.test-edit-name input', 'Jane Doe');
  fillIn('.test-edit-primary-email input', 'jdoe@gmail.com');
  fillIn('.test-edit-primary-phone input', '(818) 123-4567');
  click('.test-save-button');


  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Jane Doe', 'Contact name is updated');
    assert.equal(updatedContact.primaryEmailAddress, 'jdoe@gmail.com', 'Primary phone is updated');
    assert.equal(updatedContact.primaryPhoneNumber, '(818) 123-4567', 'Primary email is updated');
  });
});

test('Edit contact handles server error when saving contact', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });
  server.patch(`/contacts/${contact.id}`, {errors: ['Server on fire!']}, 500);

  visitEditContact(contact);

  click('.test-edit-button');
  fillIn('.test-edit-name', 'Dat boi');
  fillIn('.test-edit-primary-phone input', '(818) 123-4567');
  fillIn('.test-edit-primary-email input', 'test@example.com');
  click('.test-save-button');

  andThen(function() {
    let contact = server.db.contacts[0];
    assert.equal(contact.name, 'Ann Smith', 'Contact name is NOT updated');
    assert.equal(contact.primaryPhoneNumber, '+15555555252', 'Primary phone is NOT updated');
    assert.equal(contact.primaryEmailAddress, 'smith@example.com', 'Primary email is NOT updated');
    assert.contains('.test-error-message', 'There was an error saving this contact. Please try again.');
  });
});

skip('Edit contact handles server error when saving contact phone, saving all related models other than the erroring one', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });
  server.post(`/email-addresses`, {errors: ['Server on fire!']}, 500);

  visitEditContact(contact);

  click('.test-edit-button');
  click('.test-add-phone');
  fillIn('.test-edit-phone:last input', '(818) 123-4567');
  click('.test-add-email');
  fillIn('.test-edit-email:last input', 'test@example.com');
  click('.test-save-button');

  andThen(function() {
    assert.equal(server.db.phoneNumbers[0].value, '(818) 123-4567', 'Phone was saved');
    assert.equal(server.db.emailAddresses.length, 0, 'Email was not saved');
  });
});

skip('Edit contact handles server error when saving contact email, saving all related models other than the erroring one', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });
  server.post(`/phone-numbers`, {errors: ['Server on fire!']}, 500);

  visitEditContact(contact);

  click('.test-edit-button');
  click('.test-add-phone');
  fillIn('.test-edit-phone:last input', '(818) 123-4567');
  click('.test-add-email');
  fillIn('.test-edit-email:last input', 'test@example.com');
  click('.test-save-button');

  andThen(function() {
    assert.equal(server.db.emailAddresses[0].value, 'test@example.com', 'Email was saved');
    assert.equal(server.db.phoneNumbers.length, 0, 'Phone was not saved');
  });
});

test('Edit contact does not allow empty name', function(assert) {
  let contact = server.create('contact', { name: 'Ann Smith' });

  visitEditContact(contact);

  click('.test-edit-button');
  fillIn('.test-edit-name input', '');
  click('.test-save-button');

  andThen(function() {
    assert.equal(find('.test-error-message').text().trim(), 'Name can not be empty', 'Correct error is shown');
    assert.equal(find('.test-edit-button').length, 0, 'Empty name is not saved');
  });
});

test('Edit contact allows adding a phone', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });

  visitEditContact(contact);

  click('.test-edit-button');
  click('.test-add-phone');
  fillIn('.test-edit-phone:last input', '(818) 123-4567');
  click('.test-save-button');

  andThen(function() {
    let addedPhone = server.db.phoneNumbers[0];
    assert.equal(find('.test-contact-phone:last').text().trim(), '(818) 123-4567', 'Added phone to contact');
    assert.equal(addedPhone.value, '(818) 123-4567', 'New phone saved in db');
    assert.equal(addedPhone.contactId, contact.id, 'Added phone to correct contact');
  });
});

test('Edit contact allows adding an email', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });

  visitEditContact(contact);

  click('.test-edit-button');
  click('.test-add-email');
  fillIn('.test-edit-email:last input', 'test@example.com');
  click('.test-save-button');

  andThen(function() {
    let addedEmail = server.db.emailAddresses[0];
    assert.equal(find('.test-contact-email:last').text().trim(), 'test@example.com', 'Added email to contact');
    assert.equal(addedEmail.value, 'test@example.com', 'New email saved in db');
    assert.equal(addedEmail.contactId, contact.id, 'Added email to correct contact');
  });
});

test('Edit contact allows adding multiple phones and/or emails at the same time', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });

  visitEditContact(contact);

  click('.test-edit-button');

  click('.test-add-phone');
  fillIn('.test-edit-phone:last input', '(818) 123-4567');
  click('.test-add-phone');
  fillIn('.test-edit-phone:last input', '(818) 987-6543');

  click('.test-add-email');
  fillIn('.test-edit-email:last input', 'test@example.com');
  click('.test-add-email');
  fillIn('.test-edit-email:last input', 'test2@example.com');

  click('.test-save-button');

  andThen(function() {
    let addedEmails = server.db.emailAddresses;
    let addedPhones = server.db.phoneNumbers;

    assert.equal(find('.test-contact-phone:eq(-2)').text().trim(), '(818) 123-4567', 'Added first phone to contact');
    assert.equal(find('.test-contact-phone:eq(-1)').text().trim(), '(818) 987-6543', 'Added second phone to contact');
    assert.equal(find('.test-contact-email:eq(-2)').text().trim(), 'test@example.com', 'Added first email to contact');
    assert.equal(find('.test-contact-email:eq(-1)').text().trim(), 'test2@example.com', 'Added second email to contact');

    assert.equal(addedPhones[0].value, '(818) 123-4567', 'First phone saved in db');
    assert.equal(addedPhones[1].value, '(818) 987-6543', 'Second phone saved in db');
    assert.equal(addedEmails[0].value, 'test@example.com', 'First email saved in db');
    assert.equal(addedEmails[1].value, 'test2@example.com', 'Second email saved in db');
  });
});

test('Edit contact allows removing emails', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });
  server.create('email-address', { value: 'asmith@example.edu', contactId: contact.id });

  visitEditContact(contact);

  andThen(function() {
    assert.equal(find('.test-contact-email').length, 2);
  });

  click('.test-edit-button');
  click('.test-delete-email');
  click('.test-save-button');

  andThen(function() {
    assert.equal(find('.test-contact-email').length, 1, 'Email was deleted');
    assert.equal(find('.test-contact-email:eq(0)').text().trim(), 'smith@example.com', 'Primary email was not deleted');
    assert.equal(server.db.emailAddresses.length, 0, 'Email deleted from db');
  });
});

test('Edit contact allows removing phones', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith',
    primaryPhoneNumber: '+15555555252',
    primaryEmailAddress: 'smith@example.com'
  });
  server.create('phone-number', { value: '(818) 123-4567', contactId: contact.id });

  visitEditContact(contact);

  andThen(function() {
    assert.equal(find('.test-contact-phone').length, 2);
  });

  click('.test-edit-button');
  click('.test-delete-phone');
  click('.test-save-button');

  andThen(function() {
    assert.equal(find('.test-contact-phone').length, 1, 'Phone was deleted');
    assert.equal(find('.test-contact-phone:eq(0)').text().trim(), '+15555555252', 'Primary phone was not deleted');
    assert.equal(server.db.phoneNumbers.length, 0, 'Phone deleted from db');
  });
});

// contact is a mirage factory
function visitEditContact(contact) {
  visit(`/contacts/${contact.id}`);
}
