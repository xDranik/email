import Ember from 'ember';

export default Ember.Controller.extend({
  isEditing: false,
  errorMessage: null,

  reset() {
    this.set('isEditing', false);
    this.set('errorMessage', null);
  },

  actions: {
    edit() {
      this.set('isEditing', true);
    },
    addEmail() {
      let email = this.store.createRecord('email-address', { contactId: this.model.id });
      this.model.get('emailAddresses').pushObject(email);
    },
    addPhone() {
      let phone = this.store.createRecord('phone-number', { contactId: this.model.id });
      this.model.get('phoneNumbers').pushObject(phone);
    },
    deleteEmailAddress(emailRecord) {
      emailRecord.destroyRecord();
    },
    deletePhoneNumber(phoneRecord) {
      phoneRecord.destroyRecord();
    },
    save() {
      if (Ember.isEmpty(this.model.get('name'))) {
        this.set('errorMessage', 'Name can not be empty');
        return;
      }

      if (Ember.isEmpty(this.model.get('primaryPhoneNumber'))) {
        this.set('errorMessage', 'Primary phone number can not be empty');
        return;
      }

      if (Ember.isEmpty(this.model.get('primaryEmailAddress'))) {
        this.set('errorMessage', 'Primary email address can not be empty');
        return;
      }

      // show error if any email address is empty
      let emptyEmail = this.model.get('emailAddresses')
      .find((email) => Ember.isEmpty(email.get('value')));
      if (emptyEmail) {
        this.set('errorMessage', 'Emails can not be empty');
        return;
      }

      // show error if any email address is empty
      let emptyPhone = this.model.get('phoneNumbers')
      .find((phone) => Ember.isEmpty(phone.get('value')));
      if (emptyPhone) {
        this.set('errorMessage', 'Phone numbers can not be empty');
        return;
      }

      this.model.save()
      .then(() => {
        this.store.peekAll('email-address').save();
        this.store.peekAll('phone-number').save();
        this.reset();
      })
      .catch(() => {
        this.set('errorMessage', 'There was an error saving this contact. Please try again.');
      });
    }
  }
});
