import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.findAll('contact', { include: 'phone-numbers,email-addresses' })
    .then(contacts => contacts.sortBy('name'));
  }
});
