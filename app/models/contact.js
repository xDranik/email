import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';

export default Model.extend({
  name: attr('string'),
  primaryPhoneNumber: attr('string'),
  primaryEmailAddress: attr('string'),
  phoneNumbers: hasMany('phoneNumber'),
  emailAddresses: hasMany('emailAddress')
});
