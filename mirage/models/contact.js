import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  phoneNumbers: hasMany('phoneNumber'),
  emailAddresses: hasMany('emailAddress'),
});
