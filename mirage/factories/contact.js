import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() { return faker.name.findName(); },
  primaryPhoneNumber() { return faker.phone.phoneNumber(); },
  primaryEmailAddress() { return faker.internet.email(); }
});
